package com.breezzo.edu.chapter6.task2

object Main extends App {
    println(convert(InchesToCentimeters, 6.41))
    println(convert(GallonsToLiters, 10))
    println(convert(MilesToKilometers, 60))

    def convert(converter: UnitConversion, value: Double): Double = converter.convert(value)
}

abstract class UnitConversion {
    def convert(from: Double): Double
}

object InchesToCentimeters extends UnitConversion {
    override def convert(from: Double): Double = from * 2.54
}

object GallonsToLiters extends UnitConversion {
    override def convert(from: Double): Double = from * 3.785
}

object MilesToKilometers extends UnitConversion {
    override def convert(from: Double): Double = from * 1.609
}
