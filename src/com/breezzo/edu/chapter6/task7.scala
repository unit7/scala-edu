package com.breezzo.edu.chapter6.task7

import ColorType.ColorType
import SuitType.SuitType

object Main extends App {

    val spades = Card(SuitType.Spades)
    val clubs = Card(SuitType.Clubs)
    val hearts = Card(SuitType.Hearts)
    val diamonds = Card(SuitType.Diamonds)

    println(s"$spades is red = " + isRed(spades))
    println(s"$clubs is red = " + isRed(clubs))
    println(s"$hearts is red = " + isRed(hearts))
    println(s"$diamonds is red = " + isRed(diamonds))

    def isRed(card: Card): Boolean = SuitType.color(card.suit) == ColorType.Red
}

case class Card(suit: SuitType)

object Card {
    def apply(suit: SuitType): Card = new Card(suit)
}

object ColorType extends Enumeration {
    type ColorType = Value

    val Red: ColorType = Value
    val Black: ColorType = Value
}

object SuitType extends Enumeration {
    type SuitType = Value

    val Diamonds: SuitType = Value("♦")
    val Hearts: SuitType = Value("♥")
    val Spades: SuitType = Value("♠")
    val Clubs: SuitType = Value("♣")

    def color(suit: SuitType): ColorType = {
        suit match {
            case Diamonds => ColorType.Red
            case Hearts => ColorType.Red
            case Spades => ColorType.Black
            case Clubs => ColorType.Black
        }
    }
}