package com.breezzo.edu.chapter6.task1

object Main extends App {
    import Conversions._

    println(inchesToCentimeters(6.41))
    println(gallonsToLiters(10))
    println(milesToKilometers(60))
}

object Conversions {
    def inchesToCentimeters(inches: Double): Double = inches * 2.54

    def gallonsToLiters(gallons: Double): Double = gallons * 3.785

    def milesToKilometers(miles: Double): Double = miles * 1.609
}