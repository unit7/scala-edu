package com.breezzo.edu.chapter11.task4

object Main extends App {
    println(Money(25, 50))
    println(Money(0, 5000))
    println(Money(1, 75) + Money(0, 50))
    println((Money(1, 75) + Money(0, 50)) == Money(2, 25))
    println((Money(1, 75) + Money(0, 60)) < Money(2, 25))
    println((Money(1, 75) + Money(0, 60)) < Money(2, 250))
    println(Money(1, 750) < Money(2, 25))
    println(Money(1, 75) - Money(0, 75))
    println(Money(3, 75) - Money(2, 70))
    println(Money(3, 75) - Money(10, 70))
}

class Money private(val dollars: Int, val cents: Int) {

    def +(other: Money): Money = {
        Money(dollars + other.dollars, cents + other.cents)
    }

    def -(other: Money): Money = {
        var d = dollars - other.dollars
        var c = cents - other.cents
        if (cents < other.cents) {
            c += Money.centsInDollar
            d -= 1
        }
        Money(d, c)
    }

    def <(other: Money): Boolean = {
        dollars < other.dollars || (dollars == other.dollars && cents < other.cents)
    }

    def ==(other: Money): Boolean = {
        dollars == other.dollars && cents == other.cents
    }

    override def toString: String = "%d.%02d$".format(dollars, cents)
}

object Money {
    val centsInDollar = 100

    def apply(dollars: Int, cents: Int): Money = {
        var d = dollars
        var c = cents
        if (cents >= centsInDollar) {
            d += cents / centsInDollar
            c = cents % centsInDollar
        }
        new Money(d, c)
    }
}