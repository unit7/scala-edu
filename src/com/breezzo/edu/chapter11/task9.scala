package com.breezzo.edu.chapter11.task9


object Main extends App {
    val f = new RichFile("/home/breezzo/text.txt")
    val RichFile(path, name, extension) = f
    println(path, name, extension)

    val RichFile(path2, name2, extension2) = new RichFile("/home/breezzo/file")
    println(path2, name2, extension2)

    val RichFile(path3, name3, extension3) = new RichFile("file")
    println(path3, name3, extension3)

    val RichFile(path4, name4, extension4) = new RichFile("/file")
    println(path4, name4, extension4)

    val RichFile(path5, name5, extension5) = new RichFile("/file.txt")
    println(path5, name5, extension5)
}

class RichFile(val filename: String) {
}

object RichFile {
    def unapply(file: RichFile): Option[(String, String, String)] = {
        val filename = file.filename
        val lastSepIndex = filename.lastIndexOf('/')
        val extensionSepIndex = filename.lastIndexOf('.')
        val nameIndex = if (lastSepIndex == -1) 0 else lastSepIndex + 1
        val nameLastIndex = if (extensionSepIndex == -1) filename.length else extensionSepIndex
        val filePath = if (lastSepIndex == -1) "" else filename.substring(0, lastSepIndex + 1)
        val name = filename.substring(nameIndex, nameLastIndex)
        val extension = if (extensionSepIndex == -1) "" else filename.substring(extensionSepIndex + 1)

        Some((filePath, name, extension))
    }

    def unapplySeq(file: RichFile): Option[Seq[String]] = {
        val segments = file.filename.split("/")
        Some(segments.slice(0, segments.length - 1))
    }
}