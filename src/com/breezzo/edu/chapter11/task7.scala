package com.breezzo.edu.chapter11.task7

import scala.collection.mutable.ArrayBuffer

object Main extends App {
    val seq = BitSequence(1, 5, 7)
    println(s"(1, 5, 7) = $seq")
    println(s"seq(5) = ${seq(5)}")

    seq(2) = true
    println(s"(1, 2, 5, 7) = $seq")
    println(s"seq(3) = ${seq(3)}")
    println(s"seq(6) = ${seq(6)}")
    println(s"seq(100) = ${seq(100)}")
    seq(100) = true
    println(s"seq(100) = ${seq(100)}")
    seq(100000) = true
    println(s"(1, 2, 5, 7, 100, 100000) = $seq")

    seq(5) = false
    println(s"(1, 2, 7, 100, 100000) = $seq")
    seq(100000) = false
    println(s"(1, 2, 7, 100) = $seq")
}

class BitSequence private (private var sequence: ArrayBuffer[Long]) {

    def apply(index: Int): Boolean = {
        val longIndex = BitSequence.getLongIndex(index)
        val longRelativeBitIndex = BitSequence.getLongRelativeIndex(index)
        ensureCapacity(longIndex)

        (sequence(longIndex) & (1L << longRelativeBitIndex)) > 0
    }

    def update(index: Int, state: Boolean): Unit = {
        val longIndex = BitSequence.getLongIndex(index)
        val longRelativeBitIndex = BitSequence.getLongRelativeIndex(index)
        ensureCapacity(longIndex)

        sequence(longIndex) = getValue(sequence(longIndex), longRelativeBitIndex, state)
    }

    def getValue(current: Long, bitIndex: Int, newState: Boolean): Long = {
        if (newState) {
            current | (1L << bitIndex)
        } else {
            current & (Long.MaxValue ^ (1L << bitIndex))
        }
    }

    private def ensureCapacity(longIndex: Int): Unit = {
        (1 to ((longIndex - sequence.length) + 1))
          .foreach(_ => sequence.addOne(0))
    }

    override def toString: String = {
        var index = 0
        val res = new StringBuilder("(")
        sequence.foreach { bitStore =>
            var mask: Long = 1
            for (i <- 0 until BitSequence.longBits) {
                index += 1
                if ((bitStore & mask) > 0) {
                    if (res.length() > 1) res ++= ", "
                    res ++= index.toString
                }
                mask <<= 1
            }
        }

        res ++= ")"
        res.toString()
    }
}

object BitSequence {
    private val longBits = java.lang.Long.SIZE

    def apply(bits: Int*): BitSequence = {
        val res = new BitSequence(Array(0L).to(ArrayBuffer))
        if (bits.nonEmpty) {
            bits.foreach { bitIndex =>
                res(bitIndex) = true
            }
        }
        res
    }

    private def getLongIndex(bitIndex: Int): Int = {
        bitIndex / longBits
    }

    private def getLongRelativeIndex(bitIndex: Int): Int = {
        (bitIndex % longBits) - 1
    }
}