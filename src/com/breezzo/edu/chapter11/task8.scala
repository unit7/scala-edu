package com.breezzo.edu.chapter11.task8


object Main extends App {
    val mat = Matrix(2, 3)
    println(mat(0, 0))
    println(mat)

    mat(0, 0) = 1
    mat(1, 1) = 5
    mat(1, 2) = 564
    println(mat)

    val mat2 = Matrix(2, 3)
    mat2(0, 2) = 5
    mat2(1, 0) = -10

    println(mat + mat2)

    println(mat * 2)
}

class Matrix (private var m: Array[Array[Int]]) {

    def apply(i: Int, j: Int): Int = m(i)(j)

    def update(i: Int, j: Int, value: Int): Unit = m(i)(j) = value

    def +(other: Matrix): Matrix = convert((i, j) => m(i)(j) + other.m(i)(j))

    def *(mul: Int): Matrix = convert((i, j) => m(i)(j) * mul)

    def convert(f: (Int, Int) => Int): Matrix = {
        val res = Array.ofDim[Int](m.length, m(0).length)
        for (i <- 0 until m.length) {
            for (j <- 0 until m(i).length) {
                res(i)(j) = f(i, j)
            }
        }

        new Matrix(res)
    }

    override def toString: String = {
        val res = new StringBuilder
        m.foreach { row =>
            res ++= "| "
            row.foreach { cell =>
                res ++= "%5s".format(cell)
            }
            res ++= "    |\r\n"
        }
        res.result()
    }
}

object Matrix {
    def apply(m: Int, n: Int): Matrix = new Matrix(Array.ofDim[Int](m, n))
}