package com.breezzo.edu.chapter11.task10


object Main extends App {
    var segs = new RichFile("/home/breezzo/test/test.txt") match {
        case RichFile(seg1, seg2, "test") => (seg1, seg2)
        case _ => ()
    }
    println(segs)

    segs = new RichFile("test.txt") match {
        case RichFile(seg1, seg2, "test") => (seg1, seg2)
        case _ => ()
    }
    println(segs)
}

class RichFile(val filename: String) {
}

object RichFile {
    def unapplySeq(file: RichFile): Option[Seq[String]] = {
        val segments = file.filename.split("/").filter(_.nonEmpty)
        Some(segments.slice(0, segments.length - 1))
    }
}