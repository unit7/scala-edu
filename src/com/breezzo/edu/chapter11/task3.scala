package com.breezzo.edu.chapter11.task3

import scala.annotation.tailrec

object Main extends App {
    println(Fraction(2, 3) * Fraction(1, 5))
    println(Fraction(2, 3) + Fraction(1, 5))
    println(Fraction(2, 3) - Fraction(1, 5))
    println(Fraction(2, 3) / Fraction(1, 5))
    println(Fraction(1, 5) / Fraction(2, 3))
    println(Fraction(10, 15))
    println(Fraction(10, -15))
    println(Fraction(-10, 15))
    println(Fraction(10, -15) * Fraction(1, 5))
    println(Fraction(10, -15) * Fraction(1, -5))
}

class Fraction(n: Int, d: Int) {
    val num: Int = if (d == 0) 1 else n * sign(d) / gcd(n, d)
    val den: Int = if (d == 0) 0 else d * sign(d) / gcd(n, d)

    private def sign(v: Int): Int = if (v > 0) 1 else if (v < 0) -1 else 0

    @tailrec
    private def gcd(a: Int, b: Int): Int = if (b == 0) math.abs(a) else gcd(b, a % b)

    private def lcm(a: Int, b: Int): Int = a / gcd(a, b) * b

    def +(other: Fraction): Fraction = {
        val (selfMul, otherMul, lcmValue) = calcMultipliersForCommonDen(other)
        val resNum = num * selfMul + other.num * otherMul
        Fraction(resNum, lcmValue)
    }

    def -(other: Fraction): Fraction = {
        val (selfMul, otherMul, lcmValue) = calcMultipliersForCommonDen(other)
        val resNum = num * selfMul - other.num * otherMul
        Fraction(resNum, lcmValue)
    }

    def calcMultipliersForCommonDen(other: Fraction): (Int, Int, Int) = {
        val lcmValue = lcm(den, other.den)
        (
          lcmValue / den,
          lcmValue / other.den,
          lcmValue
        )
    }

    def *(other: Fraction): Fraction = {
        val n = num * other.num
        val d = den * other.den
        Fraction(n, d)
    }

    def /(other: Fraction): Fraction = {
        this * Fraction(other.den, other.num)
    }

    override def toString: String = s"($num / $den)"
}

object Fraction {
    def apply(n: Int, d: Int): Fraction = new Fraction(n, d)
}