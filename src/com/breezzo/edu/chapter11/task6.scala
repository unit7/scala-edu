package com.breezzo.edu.chapter11.task6

object Main extends App {
    val cat = ASCIIArt("""
       /\_/\
      ( ' ' )
      (  -  )
       | | |
      (__|__)
    """)
    val text = ASCIIArt("""
       -----
     / Hello \
    <  Scala |
     \ Coder /
       -----
    """)
    val someOne = cat :+ text :| cat :+ text
    val someOther = cat :| text :+ cat

    println(someOne)
    println()
    println(someOther)
}

class ASCIIArt(private val content: String) {

    def :+(other: ASCIIArt): ASCIIArt = {
        val contentMatrix = content.split("(\r?\n+)")
        val otherContentMatrix = other.content.split("(\r?\n+)")

        val spacesCount = 3
        val totalRowLength = {
            val maxRowLength = contentMatrix.map(_.length).max;
            maxRowLength + spacesCount
        }

        val result = new StringBuilder()

        for (i <- 0 until contentMatrix.length) {
            val leftRow = contentMatrix(i)
            val rightRow = if (i < otherContentMatrix.length) otherContentMatrix(i) else ""
            val sep = " " * (totalRowLength - leftRow.length)
            result ++= leftRow + sep + rightRow + "\r\n"
        }

        ASCIIArt(result.result())
    }

    def :|(other: ASCIIArt): ASCIIArt = {
        ASCIIArt(content + "\r\n" + other.content)
    }

    override def toString: String = content
}

object ASCIIArt {
    def apply(content: String): ASCIIArt = new ASCIIArt(content)
}