package com.breezzo.edu.chapter11.task5

object Main extends App {
    val table = Table() | "Java" | "Scala" || "Gosling" | "Odersky" || "JVM" | "JVM" | ".NET"
    println(table)
}

import Table.Row
import Table.row

class Table private (private val rows: Array[Row]) {

    def |(content: String): Table = {
        val rowsBuffer = rows.toBuffer
        if (rowsBuffer.isEmpty) {
            rowsBuffer.addOne(row(content))
        } else {
            val lastRow = rowsBuffer.last
            val newCells = lastRow.cells :+ content
            rowsBuffer(rows.length - 1) = new Row(newCells)
        }

        new Table(rowsBuffer.toArray)
    }

    def ||(content: String): Table = {
        new Table(rows :+ row(content))
    }

    override def toString: String = {
        val result = new StringBuilder("<table>")
        rows.foreach { row =>
            result ++= "<tr>"
            row.cells.foreach { cell =>
                result ++= s"<td>$cell</td>"
            }
            result ++= "</tr>"
        }
        result ++= "</tr></table>"
        result.result()
    }
}

object Table {
    def apply(): Table = new Table(Array.empty)

    private def row(cell: String): Row = new Row(Array(cell))

    private class Row private[Table] (private[Table] val cells: Array[String]) {
    }

}