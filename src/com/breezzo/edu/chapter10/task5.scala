package com.breezzo.edu.chapter10.task5

import java.awt.Point
import java.beans.{PropertyChangeEvent, PropertyChangeListener}

import scala.collection.mutable

object Main extends App {
    val p = new ListenablePoint
    p.addListener("x", new PropertyChangeListener {
        override def propertyChange(evt: PropertyChangeEvent): Unit = {
            println(s"X changed from ${evt.getOldValue} to ${evt.getNewValue}")
        }
    })

    p.move(10, 10)
    p.translate(1, 0)
    p.translate(0, 1)
    p.setLocation(25.0, 20.0)

}

trait PropertyChangeListenerSupport {
    private val propertyChangeListeners = mutable.HashMap[String, PropertyChangeListener]()

    def addListener(property: String, listener: PropertyChangeListener): Unit = {
        propertyChangeListeners(property) = listener
    }

    def firePropertyChange[T](property: String, oldVal: T, newVal: T): Unit = {
        if (oldVal != newVal)
            propertyChangeListeners.get(property)
              .foreach(_.propertyChange(new PropertyChangeEvent(this, property, oldVal, newVal)))
    }
}

class ListenablePoint extends Point with PropertyChangeListenerSupport {
    override def setLocation(x: Double, y: Double): Unit = {
        changeWithFiring(x, y, super.setLocation(_: Double, _: Double))
    }

    override def move(x: Int, y: Int): Unit = {
        changeWithFiring(x, y, super.move)
    }

    override def translate(dx: Int, dy: Int): Unit = {
        changeWithFiring(dx, dy, super.translate)
    }

    private def changeWithFiring[T](xv: T, yv: T, f: (T, T) => Unit): Unit = {
        val (oldX, oldY) = (getX, getY)
        f(xv, yv)
        firePropertyChange("x", oldX, getX)
        firePropertyChange("y", oldY, getY)
    }
}