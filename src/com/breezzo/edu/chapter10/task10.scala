package com.breezzo.edu.chapter10.task10

import java.io.{ByteArrayInputStream, InputStream}

object Main extends App {
    val is = new ByteArrayInputStream(Array(1, 2, 3, 4, 5, 6)) with IterableInputStream
    val it = is.iterator
    for (b <- it) {
        println(b)
    }
    is.close()
}

trait IterableInputStream extends Iterable[Byte] {
    this: InputStream =>

    override def iterator: Iterator[Byte] = {
        new Iterator[Byte] {
            private var lastRead = read()

            override def hasNext: Boolean = lastRead != -1

            override def next(): Byte = {
                if (lastRead == -1) throw new NoSuchElementException("There is no bytes available")

                val res = lastRead.toByte
                lastRead = read()
                res
            }
        }
    }
}