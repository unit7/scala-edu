package com.breezzo.edu.chapter10.task2

import java.awt.Point

object Main extends App {
    val points = Array(
        new OrderedPoint(10, 10),
        new OrderedPoint(10, 15),
        new OrderedPoint(10, 12),
        new OrderedPoint(11, 10),
        new OrderedPoint(8, 5),
        new OrderedPoint(8, 10),
    )
    println(points.sorted.mkString(", "))
}

class OrderedPoint(x: Int, y: Int) extends Point(x, y) with Ordered[Point] {
    override def compare(that: Point): Int = {
        if (x == that.x) y - that.y
        else x - that.x
    }

    override def toString: String = s"[$x, $y]"
}