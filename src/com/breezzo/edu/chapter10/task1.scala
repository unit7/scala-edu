package com.breezzo.edu.chapter10.task1

import java.awt.geom.Ellipse2D

object Main extends App {
    val egg = new Ellipse2D.Double(10, 10, 10, 6) with RectangleLike
    egg.translate(5, 0)
    egg.grow(2, 1)

    println(egg.getFrame)
}

trait RectangleLike {
    def getX(): Double
    def getY(): Double

    def getWidth(): Double
    def getHeight(): Double

    def setFrame(x: Double, y: Double, w: Double, h: Double): Unit

    def translate(dx: Double, dy: Double): Unit = {
        val newX = getX() + dx
        val newY = getY() + dy
        setFrame(newX, newY, getWidth(), getHeight())
    }

    def grow(w: Double, h: Double): Unit = {
        val newWidth = getWidth() + w
        val newHeight = getHeight() + h
        setFrame(getX(), getY(), newWidth, newHeight)
    }
}