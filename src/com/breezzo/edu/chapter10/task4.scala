package com.breezzo.edu.chapter10.task4

import java.time.Instant

object Main extends App {
    val defaultLogger = new ConsoleTimestampCryptoLogger
    val loggerWithFiveCypherKey = new ConsoleTimestampCryptoLogger
    loggerWithFiveCypherKey.cypherKey = 5

    defaultLogger.log("some test message")
    defaultLogger.log("another some test message")

    loggerWithFiveCypherKey.log("njh`\u001Bo`no\u001Bh`nn\\b`")
    loggerWithFiveCypherKey.log("another some test message")
}

trait Logger {
    def log(message: String): Unit = {
    }
}

trait ConsoleLogger extends Logger {
    override def log(message: String): Unit = {
        println(message)
    }
}

trait TimestampLogger extends Logger {
    override def log(message: String): Unit = {
        super.log(Instant.now() + " " + message)
    }
}

trait CryptoLogger extends Logger {
    var cypherKey = 3

    override def log(message: String): Unit = {
        val cyphered = message.map((o: Char) => (o + cypherKey).toChar)
        super.log(cyphered)
    }
}

class ConsoleTimestampCryptoLogger extends ConsoleLogger with TimestampLogger with CryptoLogger
