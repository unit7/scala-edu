package com.breezzo.edu.chapter12.task10

object Main extends App {

    val x = 1
    unlessV1(x == 2, println("done1"))
    unlessV1(x == 1, println("done2"))

    unlessV2(x == 2) {
        println("done3")
    }

    def unlessV1(condition: Boolean, body: => Unit): Unit = {
        if (!condition) {
            body
        }
    }

    def unlessV2(condition: Boolean)(body: => Unit): Unit = {
        if (!condition) {
            body
        }
    }
}