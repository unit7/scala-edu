package com.breezzo.edu.chapter12.task8

object Main extends App {

    val strings = Array("str", "test", "engine")
    val lengths = strings.map(_.length)

    println(correspondsLengths(strings, lengths))
    println(correspondsLengths(strings, Array(1, 2, 3)))

    def correspondsLengths(strings: Array[String], lengths: Array[Int]): Boolean = {
        strings.corresponds(lengths)(_.length == _)
    }
}