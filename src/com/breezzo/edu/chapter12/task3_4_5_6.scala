package com.breezzo.edu.chapter12.task3_4_5_6

object Main extends App {

    println(fact(5))
    println(fact(-5))

    println(
        largestV1(
            (x: Int) => 10 * x - x * x,
            Array(1, 5, 6, 2, 3, -9, -4, 4)
        )
    )
    println(
        largestV2(Array(1, 5, 6, 2, 3, -9, -4, 4)) { x =>
            10 * x - x * x
        }
    )
    println(
        largestV3(Array(1, 5, 6, 2, 3, -9, -4, 4)) { x =>
            10 * x - x * x
        }
    )
    println(
        largestV4(Array(1, 5, 6, 2, 3, -9, -4, 4)) { x =>
            10 * x - x * x
        }
    )

    def fact(n: Int): Int = {
        (1 to n).foldLeft(1) { _ * _ }
    }

    def largestV1(fun: Int => Int, seq: Seq[Int]): Int = {
        seq.map(fun).max
    }

    def largestV2(seq: Seq[Int])(fun: Int => Int): Int = {
        seq.map(fun).max
    }

    def largestV3(seq: Seq[Int]): (Int => Int) => Int = {
        fun => {
            seq.map(fun).max
        }
    }

    def largestV4(seq: Seq[Int])(fun: Int => Int): Int = {
        seq.map(x => (x, fun(x))).maxBy(_._2)._1
    }
}