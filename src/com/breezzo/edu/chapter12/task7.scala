package com.breezzo.edu.chapter12.task7

object Main extends App {

    println(((1 to 10) zip (11 to 20)).map(x => adjustToPair(_ + _) { x }))

    def adjustToPair(fun: (Int, Int) => Int)(t: (Int, Int)): Int = {
        fun(t._1, t._2)
    }
}