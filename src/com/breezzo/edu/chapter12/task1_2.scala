package com.breezzo.edu.chapter12.task1_2

object Main extends App {

    println(values((x: Int) => x * x, -5, 5))

    println(Array(1, 5, 7, 3, 2).reduceLeft(_ max _))

    def values(fun: Int => Int, low: Int, high: Int): Seq[(Int, Int)] = {
        (low to high).map(x => (x, fun(x)))
    }
}
