package com.breezzo.edu.chapter5
package task4


object Main extends App {
    val morning = new Time(10, 20)
    val evening = new Time(17, 20)
    val laterEvening = new Time(17, 21)

    println(s"[${morning.hours}:${morning.minutes} < ${evening.hours}:${evening.minutes}]=${morning.before(evening)}")
    println(s"[${evening.hours}:${evening.minutes} < ${morning.hours}:${morning.minutes}]=${evening.before(morning)}")
    println(s"[${evening.hours}:${evening.minutes} < ${laterEvening.hours}:${laterEvening.minutes}]=${evening.before(laterEvening)}")
    println(s"[${laterEvening.hours}:${laterEvening.minutes} < ${evening.hours}:${evening.minutes}]=${laterEvening.before(evening)}")

    new Time(25, 10)
}

class Time(hours: Int,
           minutes: Int) {
    private val minutesInHour = 60
    private val maxMinutesInDay = 24 * minutesInHour - 1;
    private val minutesFromDayStart = hours * minutesInHour + minutes

    if (minutesFromDayStart > maxMinutesInDay)
        throw new IllegalArgumentException("Hours and minutes must be in 00:00-23:59 time format")

    def before(other: Time): Boolean = {
        minutesFromDayStart < other.minutesFromDayStart
    }

    def hours(): Int = minutesFromDayStart / minutesInHour

    def minutes(): Int = minutesFromDayStart % minutesInHour
}