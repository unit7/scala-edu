package com.breezzo.edu.chapter5
package task10

object Main extends App {
    val e1 = new Employee()
    val e2 = new Employee("breezzo", 1000000000)

    println(e1.name + ":" + e1.salary)
    println(e2.name + ":" + e2.salary)
}

class Employee {
    private var _name: String = "John Q. Public"
    var salary: BigDecimal = 0.0

    def this(name: String,
             salary: BigDecimal) {
        this()
        this._name = name
        this.salary = salary
    }

    def name() = _name
}