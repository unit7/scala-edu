package com.breezzo.edu.chapter5
package task8

object Main extends App {
    val bmw = new Car("BMW", "535i", 2012);
    val skyline = new Car("Nissan", "Skyline R34", 2000, "A987CC177");
    val supra = new Car("Toyota", "Supra")
}

class Car(val vendor: String,
          val model: String,
          val year: Int = -1,
          var regNumber: String = "") {
}