package com.breezzo.edu.chapter5
package task3


object Main extends App {
    val morning = new Time(10, 20)
    val evening = new Time(17, 20)
    val laterEvening = new Time(17, 21)

    println(s"[${morning.hours}:${morning.minutes} < ${evening.hours}:${evening.minutes}]=${morning.before(evening)}")
    println(s"[${evening.hours}:${evening.minutes} < ${morning.hours}:${morning.minutes}]=${evening.before(morning)}")
    println(s"[${evening.hours}:${evening.minutes} < ${laterEvening.hours}:${laterEvening.minutes}]=${evening.before(laterEvening)}")
    println(s"[${laterEvening.hours}:${laterEvening.minutes} < ${evening.hours}:${evening.minutes}]=${laterEvening.before(evening)}")

    new Time(25, 10)
}

class Time(val hours: Int,
           val minutes: Int) {

    if (hours > 24 || hours < 0) throw new IllegalArgumentException("Hours must be in 24 time format")
    if (minutes > 59 || minutes < 0) throw new IllegalArgumentException("Minutes must be in 0-59 time format")

    def before(other: Time): Boolean = {
        hours < other.hours || (hours == other.hours && minutes < other.minutes)
    }
}