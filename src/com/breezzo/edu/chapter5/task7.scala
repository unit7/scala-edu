package com.breezzo.edu.chapter5
package task7

object Main extends App {
    val p = new Person("Fred Smith")
    println(p.firstName)
    println(p.lastName)

    val f = classOf[Person].getDeclaredField("x$1")
    f.setAccessible(true)
    println(System.identityHashCode(p.firstName))
    println(System.identityHashCode(f.get(p).asInstanceOf[Tuple2[String, String]]._1))
    println(System.identityHashCode(p.lastName))
    println(System.identityHashCode(f.get(p).asInstanceOf[Tuple2[String, String]]._2))
}

class Person(fullName: String) {
    val (firstName, lastName) = {
        val firstAndLastName = fullName.split("\\s")
        val fn = if (firstAndLastName.nonEmpty) firstAndLastName(0) else ""
        val ln = if (firstAndLastName.length > 1) firstAndLastName(1) else ""
        (fn, ln)
    }
}