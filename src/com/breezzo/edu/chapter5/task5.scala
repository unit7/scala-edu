package com.breezzo.edu.chapter5
package task5

import scala.beans.BeanProperty

object Main extends App {
    val s1 = new Student(1, "James")
    val s2 = new Student(2, "Massato")

    println(s"${s1.getId()}:${s1.getName()}")
    println(s"${s2.getId()}:${s2.getName()}")
}

class Student(@BeanProperty var id: Long,
              @BeanProperty var name: String) {

}