package com.breezzo.edu.chapter5
package task6

object Main extends App {
    val p = new Person(-5)
    println(p.age)
}

class Person(var age: Int) {
    if (age < 0) age = 0
}