package com.breezzo.edu.chapter13.task8

object Main extends App {

    println(group(Array(1, 2, 3, 4, 5, 6), 3).map(_.mkString("[", ", ", "]")).mkString("[", ", ", "]"))

    def group(arr: Array[Int], cols: Int): Array[Array[Int]] = {
        arr.grouped(cols).toArray
    }
}