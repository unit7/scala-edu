package com.breezzo.edu.chapter13.task2

object Main extends App {

    val m: Map[Char, List[Int]] = indexes("Mississippi")
    println(m)

    def indexes(str: String): Map[Char, List[Int]] = {
        str.view.zipWithIndex.foldLeft(Map[Char, List[Int]]())((m, p) => {
            val s = m.getOrElse(p._1, List[Int]()) :+ p._2
            m + (p._1 -> s)
        })
    }
}