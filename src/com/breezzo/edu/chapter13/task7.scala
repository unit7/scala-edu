package com.breezzo.edu.chapter13.task7

object Main extends App {
    val prices = Array(10, 12, 5.5, 3)
    val quantities = Array(1, 2, 3, 3)

    val f: (Double, Int) => Double = _ * _
    val total = prices.zip(quantities).map(f.tupled)
    println(total.mkString(", "))
}