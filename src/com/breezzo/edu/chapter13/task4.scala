package com.breezzo.edu.chapter13.task4_5_6


object Main extends App {

    println(filterMapValues(Array("Tom", "Fred", "Harry"), Map("Tom" -> 1, "Dick" -> 3, "Harry" -> 5)))
    println(mkString(Array("Tom", "Fred", "Harry")))
    println(mkString(Array("Tom", "Fred", "Harry"), sep = ", ", start = "[", end = "]"))

    someStrange(List(1, 2, 3, 4, 5))

    def filterMapValues(keys: Seq[String], map: Map[String, Int]): Seq[Int] = {
        keys.flatMap(key => map.get(key))
    }

    def mkString(seq: Seq[String], sep: String = ",", start: String = "", end: String = ""): String = {
        start + seq.reduceLeft(_ + sep + _) + end
    }

    def someStrange(lst: List[Int]): Unit = {
        val first = (lst :\ List[Int]())((e, acc) => acc :+ e)
        val second = (List[Int]() /: lst)((acc, e) => e +: acc)

        println(first)
        println(second)
    }
}