package com.breezzo.edu.chapter13.task10

import java.util.concurrent.{ConcurrentHashMap, Executors}

import scala.collection.concurrent.Map
import scala.collection.parallel.ExecutionContextTaskSupport
import scala.collection.parallel.mutable.ParArray
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters.ConcurrentMapHasAsScala

object Main extends App {

    val frequencies: Map[Char, Int] = new ConcurrentHashMap[Char, Int]().asScala
    val str = "abcdepostaabbbggqwqewwwewwwwwwwwwwwwwwwwwwwqqqqqqqqqqqqqqqqqqqqqqaaaaaaaaaaaaaaaaaaaaaassssssssssssssssssssssdddd"

    val parStr = ParArray(str: _*)
    val ex = Executors.newFixedThreadPool(3)
    parStr.tasksupport = new ExecutionContextTaskSupport(ExecutionContext.fromExecutor(ex))
    parStr.aggregate(frequencies)((map, c) => {
        map.updateWith(c) { prev => Some(prev.fold(1)(_ + 1)) }
        map
    }, (res, _) => res)

    val threadName = Thread.currentThread().getName
    println(s"$threadName: $frequencies")
}