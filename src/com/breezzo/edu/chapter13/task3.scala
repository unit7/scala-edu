package com.breezzo.edu.chapter13.task3

import scala.collection.mutable.ListBuffer

object Main extends App {

    println(removeZeros(List(1, 6, 0, 1, 0, 6)))

    val listBuffer = ListBuffer(1, 5, 0, 9, 0, 1, 5, 0)
    removeZeros(listBuffer)
    println(listBuffer)

    def removeZeros(lst: List[Int]): List[Int] = {
        lst.filter(_ != 0)
    }

    def removeZeros(lst: ListBuffer[Int]): Unit = {
        lst.filterInPlace(_ != 0)
    }
}