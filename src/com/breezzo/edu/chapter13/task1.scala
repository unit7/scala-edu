package com.breezzo.edu.chapter13.task1

import scala.collection.mutable

object Main extends App {

    println(indexes("Mississippi"))

    def indexes(str: String): collection.Map[Char, mutable.Set[Int]] = {
        str.view.zipWithIndex.foldLeft(mutable.HashMap[Char, mutable.Set[Int]]())((m, p) => {
            m.getOrElseUpdate(p._1, mutable.TreeSet()).add(p._2)
            m
        })
    }
}