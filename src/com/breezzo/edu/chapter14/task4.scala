package com.breezzo.edu.chapter14.task4

object Main extends App {
    val phone = Product("OnePlus 6T", 600)
    val tv = Product("Samsung TV", 2000)
    val pen = Product("Pen", 1.5)
    val rice = Product("Rice", 1.8)
    val products = Bundle("Products", rice, Multiple(5, pen))
    val tech = Bundle("Techs", phone, tv)
    val total = Bundle("Total", Multiple(2, tech), products)

    println(total.price)
    println(total)
}

abstract class Item {
    def price: Double
}

case class Product(description: String, price: Double) extends Item

case class Bundle(description: String, items: Item*) extends Item {
    override def price: Double = items.map(_.price).sum
}

case class Multiple(multiplier: Int, item: Item) extends Item {
    override def price: Double = multiplier * item.price
}