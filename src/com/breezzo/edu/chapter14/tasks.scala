package com.breezzo.edu.chapter14.tasks

object Main extends App {

    println(swap((10, 20)))

    println(swap(Array(1)).mkString(", "))
    println(swap(Array(1, 2)).mkString(", "))
    println(swap(Array(1, 2, 3)).mkString(", "))
    println(swap(Array(1, 2, 3, 4)).mkString(", "))

    val lst = ((3 :: 8 :: Nil) :: 2 :: Nil) :+ (5 :: Nil)
    println(leafSum(lst))

    val tree = Node(Plus, Node(Mul, Leaf(3), Leaf(8)), Leaf(2), Node(Minus, Leaf(5)))
    println(leafSum(tree))

    println(sum(None :: Some(3) :: Some(5) :: None :: Some(1) :: Nil))

    println(compose(f, g)(2))
    println(compose(f, g)(1))
    println(compose(f, g)(0))

    def swap(p: (Int, Int)): (Int, Int) = p match {
        case (x, y) => (y, x)
    }

    def swap(a: Array[Int]): Array[Int] = a match {
        case Array(x, y, rest @ _*) => Array(y, x) ++ rest
        case _ => a
    }

    def leafSum(lst: List[Any]): Int = lst match {
        case Nil => 0
        case ::(h: Int, t: List[_]) => h + leafSum(t)
        case ::(h: List[_], t: List[_]) => leafSum(h) + leafSum(t)
    }
    
    def leafSum(tree: Tree): Int = tree match {
        case Leaf(value) => value
        case Node(op, childNodes @ _*) => childNodes.view.map(leafSum).foldLeft(op.init)(op.exec)
    }

    def sum(lst: List[Option[Int]]): Int = lst.flatten.sum

    def f(x: Double): Option[Double] = if (x == 0) None else Some(math.sqrt(x))
    def g(x: Double): Option[Double] = if (x != 1) Some(1 / (x - 1)) else None

    def compose(x: Double => Option[Double], y: Double => Option[Double])(p: Double): Option[Double] = {
        x(p).flatMap(y)
    }
}

abstract class Tree
case class Leaf(value: Int) extends Tree
case class Node(op: Operator, childNodes: Tree*) extends Tree

sealed abstract class Operator {
    def exec(x: Int, y: Int): Int
    def init: Int
}

case object Plus extends Operator {
    override def exec(x: Int, y: Int): Int = x + y
    override def init: Int = 0
}

case object Minus extends Operator {
    override def exec(x: Int, y: Int): Int = x - y
    override def init: Int = 0
}

case object Mul extends Operator {
    override def exec(x: Int, y: Int): Int = x * y
    override def init: Int = 1
}
