package com.breezzo.edu.chapter17.tasks

import com.breezzo.edu.chapter17.tasks.task1.{Pair => ImmutablePair}
import com.breezzo.edu.chapter17.tasks.task2.{Pair => MutablePair}
import com.breezzo.edu.chapter17.tasks.task10.{Pair => SamePair}

import scala.collection.mutable

object Main extends App {
    val p = new ImmutablePair(1, "String")
    val pReversed1: ImmutablePair[String, Int] = p.reversed
    val pReversed2: ImmutablePair[String, Int] = ImmutablePair.swap(p)
    println(p)
    println(pReversed1)
    println(pReversed2)

    val mp = new MutablePair(1, "String")
    mp.swap()

    println(mp)

    println(middle("World"))

    val susan = new Student("Susan")
    val fred = new Person("Fred")

    makeFriendWith(susan, fred)

    val sp: SamePair[String, Double] = new SamePair("K24A3", 2.4)
    val sp1: SamePair[String, String] = new SamePair("K24A3", "Turbo")
    // sp.reversed error!
    val sp2 = sp1.reversed
    println(sp1, sp2)

    def makeFriendWith(s: Student, f: Friend[Student]): Unit = f.befriend(s)

    def middle[T](it: Iterable[T]): T = it.drop(it.size / 2).head
}

package task1 {

    final class Pair[T, S](val first: T, val second: S) {
        def reversed: Pair[S, T] = new Pair(second, first)

        override def toString: String = s"($first, $second)"
    }

    object Pair {
        def swap[T, S](p: Pair[T, S]): Pair[S, T] = new Pair(p.second, p.first)
    }

}

package task2 {

    class Pair[T](var first: T, var second: T) {
        def swap(): Unit = {
            val tmp = first; first = second; second = tmp
        }

        override def toString: String = s"($first, $second)"
    }

}

package task10 {
    final class Pair[T, S](val first: T, val second: S) {
        def reversed(implicit ev: T =:= S): Pair[S, T] = new Pair(second, first)

        override def toString: String = s"($first, $second)"
    }
}

trait Friend[-T] {
    def befriend(someone: T): Unit
}

class Person(val name: String) extends Friend[Person] {
    val friends = new mutable.HashSet[Person]()

    override def befriend(someone: Person): Unit = friends.addOne(someone)

    override def toString: String = s"$name"
}

class Student(name: String) extends Person(name)