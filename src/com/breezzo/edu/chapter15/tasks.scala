package com.breezzo.edu.chapter15.tasks

import java.io.IOException

import scala.annotation.{tailrec, varargs}
import scala.io.Source

object Main extends App {

    factorial(-1)

    @varargs
    def sum(args: Int*): Int = args.sum

    @throws(classOf[IOException])
    def read(filename: String): String = Source.fromFile(filename).mkString

    def allDifferent[@specialized(Int) T](v1: T, v2: T, v3: T): Boolean = v1 != v2 && v2 != v3 && v1 != v3

    @tailrec
    def factorial(n: Int): Int = {
        assert(n >= 0, "Only positive numbers allowed")
        if (n == 0) 1 else factorial(n - 1)
    }
}

@deprecated
class AnnotatedClass(@deprecated val s: String) {

    def f(): Unit = ???

    @deprecated
    def f1(): Unit = ???
}

class ConcurrentResource(@volatile var vf: Boolean) {
}