package com.breezzo.edu.chapter15;

import java.io.IOException;

public class JavaMain {

    public static void main(String[] args) throws IOException {
        int sum = com.breezzo.edu.chapter15.tasks.Main.sum(1, 2, 3, 5);
        System.out.println(sum);

        String s = com.breezzo.edu.chapter15.tasks.Main.read("C:/users/breezzo/ovpntray.log");
        System.out.println(s);
    }
}
